# Jigsaw Toxic Classification Challenge https://www.kaggle.com/c/jigsaw-toxic-comment-classification-challenge

This repository contains some of my code used for this challenge. The challenge is to detect different type of offencive (toxic) comments scraped from the internet. Each comment can have more than one toxic label.

My team GroNNings ranked in the top 5% out of the 4551 teams that were considered for the finals ranking. 
The repository does not contain the word-vector models and the predictions of any of the base or stacked classifiers due to the large size of the files. The initial challenge data can be found in the Kaggle website, at the link above.

## Structure of the repository

### Code
Contains the Python scripts which were used for training various Neural Network architectures (RNNs, CNNs, Attention, hybrids etc..). Also contains cutsom Attention layers and custom Keras callbacks. The training scripts include train/validation splitting, fit_generators and predictors, and out-of-fold training and predictions. 

### fasttext
Contains scripts in which I am using the original (C) fastText module for creating vectors out of words, and for simple predictions (creating features for higher level models). 

### notebooks
Contains Jupyter notebooks in which I am exploring different Neural Network architectures, the behaviour of different embeddings, and general testing. 

### Turi
Contains some scripts in which I tried to use Apple's [turicreate](https://github.com/apple/turicreate). At the time of usage, turicreate was just open-sourced, and I did not find it suitable for this project, although it does show a lot of promise.

### WordCounts
Contains scripts in which I create predictions using word counts (tf-idf) models. One of the scripts is using gradient boosted trees ensembler method, the other a set of linear regressions (Naive Bayes, Logistic Regression, Ridge Regressino) in order to create a several low level predictions for later stacking (weak lerners). For the time it took to create and run these models, they did excellent job!

### xplots
Contains the some of the Neural Networks architectures tried and used in the competition, as well as the training and validation loss metrics as a function of epoch.

### Stacking 
Contains two notebooks that use gradient boosted trees (one via [xgboost](https://xgboost.readthedocs.io/en/latest/model.html), one via [lightgbm](https://github.com/Microsoft/LightGBM) ) as a meta-classifier for stacking of the outputs from the various neural network models. This was a good exercise to try, but ultimately another approach was adopted.

### Bagging
This folder contains notebooks that do several types of averaging of the predictions from the Neural Network models. The Bagging notebook contains a method for averaging where I am trying to optimize the ROC score, by iteratively rejecting model predictions that are not useful Similar thing is done witha  Ranking Average. Also there is a script that comutes the correlations between the different models, in order to find an optimal way of averaging/stacking/bagging the different families of Neural Network models.

### LevelUp
This folder contains the final/highest level stacking model, which takes all best models (some already stacked/bagged from before), from the Neural Networks, Word Couts, and fastText approach to craete the final prediciton. There are also few scripts that create additional features based on the comments text. 

## Final words

This was a fun challenge to participate it. I definetely learned a lot by doing it, and I would recommend anyone interested in machine learning applications to try a Kaggle challenge at one point. The community is also welcoming and encouraging. 
On a different note.. while this challenge was very fun to do from a technical stand point.. it did raise some ethical questions regarding free speech. At the end, some of the top data science/machine learning minds in the world were asked to create models to classify text...

