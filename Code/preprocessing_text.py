import re
import string


### Text preprocessing: cleaning from junk that we don't expect to need
def preprocessing_text(text, 
                       url=True, 
                       html=True, 
                       newlines=True, 
                       lowercase=True, 
                       punct=True,
                       numbers=True,
                       nonascii=True,
                       fillempty=True,
                       tokenize=True):
    '''
    My own text preprocessort for fastText.
    
    arguments:
    text = a text string to be cleaned
    '''    
    
    # Remove URLS (not 100% effective as far as I know, but it does a decent job)
    if url:
        text = re.sub(r'http\S+', '', text)
        text = re.sub(r'''(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))''', '', text)
        
    # Remove HTML markup
    if html:
#         text = BeautifulSoup(text, features='lxml').get_text()
        text = text.replace("&amp;", "and").replace("&gt;", ">").replace("&lt;", "<")
    
    # Remove new lines
    if newlines:
        text = text.strip().replace("\n", " ").replace("\r", " ")
        
    # Make all strings lower case
    if lowercase:
        text = text.lower()
            
    # Remove numbers (the double pass is needed.. for some reason)
    digits = re.compile('[%s]' % re.escape(string.digits))
    if numbers:
        text = digits.sub(' ', text)
        text = ' '.join(text.split())
        # text = re.sub("^\d+\s|\s\d+\s|\s\d+$", " ", text)
    
#     # Replace punctuation with a whitespace
#     punctuation = re.compile('[%s]' % re.escape(string.punctuation))
#     if punct:
#         text = punctuation.sub(' ', text)
#         text = ' '.join(text.split())
    
    # Isolate punctuation
    if punct:
        text = re.sub(r'([\!\"\#\$\%\&\\\'\(\)\*\+\,\-\.\/\:\;\<\=\>\?\@\[\\\\\]\^\_\`\{\|\}\~])', r' \1 ', text)
        text = ' '.join(text.split())
    
    # Remove all non-ascii characters
    if nonascii:
        text = re.sub(r'[^\x00-\x7F]','', text)
        text = ' '.join(text.split())
    
    # Fill if empty
    if fillempty:
        if len(text) == 0:
            text = 'blank line'
        
    # Tokenize the text
    if tokenize:
        # text = spacy_parser(text)
        text = str(text).split()
    return text

####### OLD VERSION ########################################################
# ### Text preprocessing: cleaning from junk that we don't expect to need
# def preprocessing_text(text,
#                        url=True,
#                        html=True,
#                        newlines=True,
#                        twitter=True,
#                        lowercase=True,
#                        punct=True,
#                        numbers=True,
#                        tokenize=True):
#     '''
#     My own text preprocessort. To be passed to TfidfVectorizer or to be used as
#     a stand along thing to clean text up.

#     arguments:
#     text = a text string to be cleaned

#     '''
#     # Remove URLS (not 100% effective as far as I know, but it does a decent job)
#     if url:
#         text = re.sub(r'http\S+', '', text)
#         text = re.sub(r'''(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))''', '', text)

#     # Remove HTML markup
#     if html:
# #         text = BeautifulSoup(text, features='lxml').get_text()
#         text = text.replace("&amp;", "and").replace("&gt;", ">").replace("&lt;", "<")

#     # Remove new lines
#     if newlines:
#         text = text.strip().replace("\n", " ").replace("\r", " ")

#     # Remove twitter mentions
#     if twitter:
#         mentionFinder = re.compile(r"@[a-z0-9_]{1,15}", re.IGNORECASE)
#         hashtagFinder = re.compile(r"#[a-z0-9_]{1,15}", re.IGNORECASE)
#         text = mentionFinder.sub("@MENTION", text)
#         text = hashtagFinder.sub("#HASHTAG", text)

#     # Make all strings lower case
#     if lowercase:
#         text = text.lower()

#     # Replace punctuation with a whitespace
#     punctuation = re.compile('[%s]' % re.escape(string.punctuation))
#     if punct:
#         text = punctuation.sub(' ', text)
#         text = ' '.join(text.split())

#     # Remove numbers (the double pass is needed.. for some reason)
#     if numbers:
#         text = re.sub("^\d+\s|\s\d+\s|\s\d+$", " ", text)
#         text = re.sub("^\d+\s|\s\d+\s|\s\d+$", " ", text)
#         text = re.sub("^\d+\s|\s\d+\s|\s\d+$", " ", text)

#     # Tokenize the text
#     if tokenize:
#         # text = spacy_parser(text)
#         text = str(text).split()
#     return text