################# BEGIN TOXIC ##################################################

import matplotlib
matplotlib.use('Agg')

import re
import gc
import string
import gensim
import datetime
import pylab as p
import keras as K
from numpy import *
import numpy as np
import pandas as pd
from tqdm import tqdm
from sklearn.model_selection import KFold
from preprocessing_text import preprocessing_text
from sklearn.model_selection import train_test_split
from CustomKerasCallbacks import RocAucMetricCallback
from sklearn.metrics import log_loss as sklearn_log_loss
from AttentionLayer import Attention, AttentionWithContext
from sklearn.metrics import roc_auc_score as sklearn_roc_auc_score

################# Read in the data #############################################
print('Reading in the competition data...')

# Read in the TOXIC competition data
train = pd.read_csv('../data/train.csv')
test = pd.read_csv('../data/test.csv')

# # Augmentation data
# de = pd.read_csv('./data/train_de.csv')
# fr = pd.read_csv('./data/train_fr.csv')
# es = pd.read_csv('./data/train_es.csv')
# # Concat the data for training
# train = pd.concat([train, de, fr, es])

# Single out the text
X = train.comment_text.fillna(value='unknown').values
Y = test.comment_text.fillna(value='unknown').values

# Target labels
ycols = train.columns[2:]
ys = train[ycols].values


print('Reading the challenge data...')
print( '%-27s %-10i' % ('Size of the training set:', len(X)))
print( '%-27s %-10i' % ('Size of the testing set:', len(Y)))
print()

################# Text pre-processing ##########################################
print('Text pre-processing...')
print()

# Preprocessing the train set
X_clean = zeros_like(X)
for i,v in enumerate(tqdm(X, desc='preprocessing the training set...')):
    X_clean[i] = preprocessing_text(v, lowercase=True)

# Preprocessing the test set
Y_clean = zeros_like(Y)
for i,v in enumerate(tqdm(Y, desc='preprocessing the testing set...')):
    Y_clean[i] = preprocessing_text(v, lowercase=True)

################# The word embeddings model ####################################
print('Reading the word embeddings model...')
print()

word_model = gensim.models.KeyedVectors.load_word2vec_format('../word_embeddings/fasttext-crawl', binary=True)


################# Encoding the input data into word vectors ####################
print('Encoding the input data into word vectors...')
print()

class pre_embeddings(object):
    def __init__(self):
        pass

    def word2idx(self, word):
        try:
            return word_model.wv.vocab[word].index
        except:
            return 0
            # return random.randint(0, 2519370) #0

    def word2vector(self, word):
        try:
            return word_model[word]
        except:
            return word_model['blank']
        
    def idx2word(self, idx):
        return word_model.wv.index2word[idx]
    
    def prepare_embeddings(self, data, max_sentence_len=150):
        '''
        params:
        data = an array containing lists of sentences
        max_sentence_len = int, maximum number of words a sentence can have
        returns:
        res = an array with the word indices
        '''

        res = zeros((len(data), max_sentence_len), dtype=int32)
        for i,v in enumerate(data):
            for j, w in enumerate(v[:max_sentence_len]):
                res[i, j] = self.word2idx(w)
        return res
    
    def prepare_vectors(self, data, max_sentence_len=150):
        '''
        params:
        data = an array containing lists of sentences
        max_sentence_len = int, maximum number of words a sentence can have
        returns:
        res = an array with the word indices
        '''

        res = zeros((len(data), max_sentence_len, 300), dtype=float32)
        for i,v in enumerate(data):
            for j, w in enumerate(v[:max_sentence_len]):
                res[i, j] = self.word2vector(w)
        return res

# NN batch generator that also does the word embedding bit
def nn_batch_generator(X_data, y_data, batch_size):
    samples_per_epoch = X_data.shape[0]
    number_of_batches = samples_per_epoch/batch_size
    counter=0
    index = arange(samples_per_epoch)
    prepare = pre_embeddings()
    
    if y_data is not None:
        while 1:
            index_batch = index[batch_size*counter:batch_size*(counter+1)]
            X_batch = X_data[index_batch]
            X_batch = prepare.prepare_vectors(X_batch, max_sentence_len=max_sentence_len)
            y_batch = y_data[index_batch]
            counter += 1
            yield array(X_batch),y_batch
            if (counter > number_of_batches):
                counter=0
    else:
        while 1:
            index_batch = index[batch_size*counter:batch_size*(counter+1)]
            X_batch = X_data[index_batch]
            X_batch = prepare.prepare_vectors(X_batch, max_sentence_len=max_sentence_len)
            counter += 1
            yield array(X_batch)
            if (counter > number_of_batches):
                counter=0 


################# Defining the Neural Network Model ############################
print('Defining the Neural Network model...')
print()

def NN():

    # Input
    inp = K.layers.Input(shape=(max_sentence_len, 300), name='input')
    
    # GRU
#     x = K.layers.SpatialDropout1D(rate=0.3, name='drop1')(inp)
    x = K.layers.Bidirectional(K.layers.CuDNNGRU(units=96, return_sequences=True), name='GRU1')(inp)
    x = K.layers.SpatialDropout1D(rate=0.3, name='drop1')(x)

    # Global Poolings
    x1 = K.layers.GlobalMaxPooling1D(name='pool_max')(x)
    x2 = K.layers.GlobalAveragePooling1D(name='pool_avg')(x)
    x3 = AttentionWithContext(name='attention')(x)
    
    # Concatinate the pooling layers
    x  = K.layers.concatenate([x1, x2, x3], name='concat')
    
#     # Dense
#     x = K.layers.Dense(units=64, activation=K.activations.relu, name='dense1')(x)
#     x = K.layers.Dropout(rate=0.1, name='drop9')(x)

    # Output
    x = K.layers.Dense(units=6, activation=K.activations.sigmoid, name='output')(x)

    # This defines the NN Model, sets of inputs and outputs
    nn = K.models.Model(inputs=inp, outputs=x)

    # Compile the NN
    nn.compile(optimizer=K.optimizers.RMSprop(lr=0.001, decay=0.0005, clipvalue=1, clipnorm=1),
               loss=K.losses.binary_crossentropy,
               metrics=[K.losses.binary_crossentropy, K.metrics.binary_accuracy])

    # Good luck!
    return nn

# The NN model function
nn = lambda: NN()

################# Training settings ############################################
print('Loading training settings...')
print()

epochs     = 4 # 31 # 17
batch_size = 128
model_id   = 'nn_jovan_fasttext-crawl_GRU96_concatMaxPoolAvgPoolAttentionWithContext_lrdecay_short'
verbose    = 1
max_sentence_len = 256
prepare = pre_embeddings()

print('Training model', model_id)
print()

# Print the graph design to a file
print('Saving the graph design to file...')
print()
K.utils.plot_model(nn(), to_file='../xplots/graph_' + model_id + '.png', show_shapes=True)

################# The training function ########################################
print('Defining the training function...')
print()

def training_day(model, model_id, XX, yx, VV, yv, YY, epochs=2, batch_size=128, max_sentence_len=256, verbose=1):
        
    # Preparing the validation data
    VV_emb = prepare.prepare_vectors(VV, max_sentence_len=max_sentence_len)
    # Preparing the training data
    train_steps_per_epoch = int(ceil(XX.shape[0]/batch_size))
    train_gen = nn_batch_generator(XX, yx, batch_size=batch_size)
    # Preparing the testing data
    test_steps_per_epoch = int(ceil(YY.shape[0]/batch_size))
    test_gen = nn_batch_generator(YY, None, batch_size=batch_size)
    
    # Callbacks
    roccb         = RocAucMetricCallback(predict_batch_size=batch_size)
    early         = K.callbacks.EarlyStopping(monitor='val_roc_auc', patience=10, verbose=1, mode='max', min_delta=0.0001)
    model_weights = '../xmodel_weights/' + model_id + '.hdf'
    checkpoint    = K.callbacks.ModelCheckpoint(model_weights, monitor='val_roc_auc', verbose=0, save_best_only=True, mode='max')
    decay         = K.callbacks.ReduceLROnPlateau(monitor='val_roc_auc', factor=0.2, patience=4, 
                                                  verbose=1, mode='max', epsilon=0.001)
    # The callback list
    callback_list = [roccb, early, checkpoint] # [early, checkpoint, decay]
    
    history = None
    # The training happens here
    history = model.fit_generator(train_gen, steps_per_epoch=train_steps_per_epoch, epochs=epochs, verbose=1,
                               validation_data=(VV_emb, yv), callbacks=callback_list)

    # Get the best weights of the model once the training is complete
    model.load_weights(model_weights)

    # Make prediction based on the validation and get the mean val_roc for this fold
    vvpred   = model.predict(VV_emb, batch_size=batch_size, verbose=verbose)
    val_roc  = sklearn_roc_auc_score(yv, vvpred)
    val_loss = array(history.history['val_loss']).min()

    # The predictions for the test set
    pred = model.predict_generator(test_gen, steps=test_steps_per_epoch, verbose=verbose)

    # Clean the TF graph and create a new one
    K.backend.clear_session()
    
    # Call the garbage collector a few times
    for i in range(10):
        gc.collect()

    # That's it folks
    return history, vvpred, val_roc, val_loss, pred

################# Training with x-validation ###################################
print('Training the model with x-validation...')

# The Kfold generator
num_folds = 8
kf = KFold(n_splits=num_folds, shuffle=False, random_state=None)

# The Histories of the training process
hh = zeros(num_folds, dtype='O')

# The predictions from the validation folds
vvpred   = zeros(num_folds, dtype='O')

# The validation roc-auc for each fold
val_roc  = zeros(num_folds)

# The validation loss for each fold
val_loss = zeros(num_folds)

# The test results predictions from each fold
pred = zeros((num_folds, 153164, 6))

# Training start time
start_dt = datetime.datetime.now()

# The master loop: looping over each fold
for i,(train_ind, val_ind) in enumerate(kf.split(X, ys)):
    print()
    print('Training on fold %i/%i:' % (i+1, num_folds))
    # The train and validation data split
    XX, yx = X_clean[train_ind], ys[train_ind]
    VV, yv = X_clean[val_ind], ys[val_ind]

    # The model id of the specific fold
    model_id_fold = model_id + '-'+str(i)

    hh[i], vvpred[i], val_roc[i], val_loss[i], pred[i] = training_day(model=nn(), model_id=model_id_fold,
                                                                      XX=XX, yx=yx, VV=VV, yv=yv, YY=Y_clean,
                                                                      epochs=epochs, batch_size=batch_size,
                                                                      max_sentence_len=max_sentence_len,
                                                                      verbose=verbose)
# Training end time
end_dt = datetime.datetime.now()
print('The training session lasted', end_dt - start_dt)
print()

# Call the garbage collector a few times
for i in range(10):
    gc.collect()


################# Results of the process #######################################
print('Gathering results from the process...')
print()

# Make the figure displaying the loss
p.figure(figsize=(9,6))
for i in range(len(ycols)):
    for h in hh:
        p.plot((h.history['loss']), lw=2, ls='solid', label='train')
        p.plot((h.history['val_loss']), lw=2, ls='dashed', label='val')
# p.legend(frameon=False)
p.xlabel('Epoch')
p.ylabel('loss')
p.savefig('../xplots/' + model_id + '.pdf', bbox_inches='tight')


# List the loss and the roc-auc for each fold
for i in range(num_folds):
    print('Validation logloss and roc-auc for fold %i:, %2.8f | %2.8f' % (i+1, val_loss[i], val_roc[i]))
# The average loss and roc-auc for this particular model
print()
print('Average logloss across the x-validation folds: %2.8f' % (val_loss.mean()))
print('Average roc-auc across the x-validation folds: %2.8f' % (val_roc.mean()))

# Save the average model performance to a file on disk
fout = open('../xmodel_list.txt','a')
print(model_id+':', val_loss.mean(), val_roc.mean(), file=fout)
fout.close()

# Stack and save to disk the prediction for the training set from the x-validation folds
vvpred = vstack(vvpred)
valid = pd.DataFrame(vvpred, columns=ycols)
valid.to_csv('../xvalidations/' + model_id + '.csv.bz', index=False, compression='bz2')

# Averaging out the predictions for the test set from all folds, and saving it to disk
pred = pred.mean(axis=0)

# Creating the submission file
submission = pd.read_csv('../sample_submission.csv')
submission[ycols] = pd.DataFrame(pred, columns=ycols)
submission.to_csv('../xsubmissions/' + model_id + '.csv', index=False)


################# END OF TOXIC #################################################
print()
print('The rent is too DAMN high!!!')
print()

