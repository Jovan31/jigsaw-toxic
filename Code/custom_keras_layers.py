import keras as K

class Attention(K.layers.Layer):
    def __init__(self, step_dim,
                 W_regularizer=None, b_regularizer=None,
                 W_constraint=None, b_constraint=None,
                 bias=True, **kwargs):
        '''
        Keras Layer that implements an Attention mechanism for temporal data.
        Supports Masking.
        Follows the work of Raffel et al. [https://arxiv.org/abs/1512.08756]
        # Input shape
            3D tensor with shape: `(samples, steps, features)`.
        # Output shape
            2D tensor with shape: `(samples, features)`.
        :param kwargs:
        Just put it on top of an RNN Layer (GRU/LSTM/SimpleRNN) with return_sequences=True.
        The dimensions are inferred based on the output shape of the RNN.
        Example:
            model.add(LSTM(64, return_sequences=True))
            model.add(Attention())
        '''

        self.supports_masking = True
        self.init = K.initializers.get('glorot_uniform')

        self.W_regularizer = K.regularizers.get(W_regularizer)
        self.b_regularizer = K.regularizers.get(b_regularizer)

        self.W_constraint = K.constraints.get(W_constraint)
        self.b_constraint = K.constraints.get(b_constraint)

        self.bias = bias
        self.step_dim = step_dim
        self.features_dim = 0
        super(Attention, self).__init__(**kwargs)

    def build(self, input_shape):
        assert len(input_shape) == 3
        # The weights
        self.W = self.add_weight((input_shape[-1],),
                                 initializer=self.init,
                                 name='{}_W'.format(self.name),
                                 regularizer=self.W_regularizer,
                                 constraint=self.W_constraint)
        self.features_dim = input_shape[-1]
        # The bias
        if self.bias:
            self.b = self.add_weight((input_shape[1],),
                                     initializer='zero',
                                     name='{}_b'.format(self.name),
                                     regularizer=self.b_regularizer,
                                     constraint=self.b_constraint)
        else:
            self.b = None
        # Building
        self.built = True

    def compute_mask(self, input, input_mask=None):
        # Do not pass the mask to the next layers
        return None

    def call(self, x, mask=None):

        features_dim = self.features_dim
        step_dim = self.step_dim

        eij = K.backend.reshape(K.backend.dot(K.backend.reshape(x, (-1, features_dim)), K.backend.reshape(self.W, (features_dim, 1))), (-1, step_dim))

        if self.bias:
            eij += self.b

        eij = K.backend.tanh(eij)

        a = K.backend.exp(eij)

        # apply mask after the exp. will be re-normalized next
        if mask is not None:
            # Cast the mask to floatX to avoid float64 upcasting in theano
            a *= K.backend.cast(mask, K.backend.floatx())

        # in some cases especially in the early stages of training the sum may be almost zero
        a /= K.backend.cast(K.backend.sum(a, axis=1, keepdims=True) + K.backend.epsilon(), K.backend.floatx())

        a = K.backend.expand_dims(a)
        weighted_input = x * a
    #print weigthted_input.shape
        return K.backend.sum(weighted_input, axis=1)

    def compute_output_shape(self, input_shape):
        #return input_shape[0], input_shape[-1]
        return input_shape[0],  self.features_dim