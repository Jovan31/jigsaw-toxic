from keras.callbacks import Callback
from sklearn.metrics import roc_auc_score as sklearn_roc_auc_score

class RocAucMetricCallback(Callback):
  def __init__(self, predict_batch_size=32, include_on_batch=False):
    super(RocAucMetricCallback, self).__init__()
    self.predict_batch_size=predict_batch_size
    self.include_on_batch=include_on_batch

  def on_batch_begin(self, batch, logs={}):
    pass

  def on_batch_end(self, batch, logs={}):
    pass
    # if(self.include_on_batch):
    #   logs['val_roc_auc']=float('-inf')
    #   if(self.validation_data):
    #     logs['val_roc_auc']=sklearn_roc_auc_score(self.validation_data[1],
    #                                       self.model.predict(self.validation_data[0],
    #                                                          batch_size=self.predict_batch_size))

    def on_train_begin(self, logs={}):
      if not ('val_roc_auc' in self.params['metrics']):
        self.params['metrics'].append('val_roc_auc')

  def on_train_end(self, logs={}):
    pass

  def on_epoch_begin(self, epoch, logs={}):
    pass

  def on_epoch_end(self, epoch, logs={}):
    logs['val_roc_auc']=float('-inf')
    if(self.validation_data):
      logs['val_roc_auc']= myroc = sklearn_roc_auc_score(self.validation_data[1],
                                                         self.model.predict(self.validation_data[0],
                                                                            batch_size=self.predict_batch_size))
      print(' - val_roc_auc: %.4f \n' %  myroc)